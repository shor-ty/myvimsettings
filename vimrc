set tabstop=4 shiftwidth=4 expandtab
execute pathogen#infect()

syntax on
filetype plugin indent on
let g:NERDTreeDirArrows=0

set t_Co=256

colorscheme xoria

command BuildAFC execute ":wa | !make"
map <F5> :BuildAFC<CR>

"- vim Extension OpenFOAM
filetype plugin indent on
let g:foam256_use_custom_colors=1

"autocmd BufWritePre * :%s/\s\+$//e
